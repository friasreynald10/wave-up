Great news for Android 9 users! WaveUp now uses a new Accessibility Services method to lock your device, which makes the fingerprint unlock feature work again, yay!
You'll have to re-enable the 'lock' option from the app again.

New in 3.0.2
★ Add crash reporting support. Reports can be sent per email. Only if the user chooses to!
★ Update some dependencies.

New in 3.0.1-beta
★ Only Android 9: Fix 'unable to unlock using fingerprint' bug.
★ Remove 'revoke device admin' menu item.